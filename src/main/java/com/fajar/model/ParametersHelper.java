package com.fajar.model;

import java.io.Serializable;

public class ParametersHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1046550671759525267L;
	
	private String need;
	private String name;
	private String value;
	private String description;
	public String getNeed() {
		return need;
	}
	public void setNeed(String need) {
		this.need = need;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}

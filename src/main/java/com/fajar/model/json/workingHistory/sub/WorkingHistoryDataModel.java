package com.fajar.model.json.workingHistory.sub;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class WorkingHistoryDataModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -487216390140515095L;

	@JsonProperty("work_id")
	private String workId;
	
	@JsonProperty("company_name")
	private String companyName;
	
	@JsonProperty("position")
	private String position;
	
	@JsonProperty("join_date")
	private String joinDate;
	
	@JsonProperty("resign_date")
	private String resignDate;
	
	@JsonProperty("description")
	private String description;

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	public String getResignDate() {
		return resignDate;
	}

	public void setResignDate(String resignDate) {
		this.resignDate = resignDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

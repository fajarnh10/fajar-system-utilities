package com.fajar.model.json.workingHistory;

import java.util.List;

import com.fajar.model.json.workingHistory.sub.WorkingHistoryDataModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class GeneralWorkingHistoryModelResponse {

	@JsonProperty("status")
	private String status;
	
	@JsonProperty("data")
	private List<WorkingHistoryDataModel> data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<WorkingHistoryDataModel> getData() {
		return data;
	}

	public void setData(List<WorkingHistoryDataModel> data) {
		this.data = data;
	}
}

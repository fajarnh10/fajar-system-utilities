package com.fajar.model.json.workingHistory;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class GeneralWorkingHistoryModelRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1815315245709314271L;
	
	@JsonProperty("working_id")
	private String workingId;

	public String getWorkingId() {
		return workingId;
	}

	public void setWorkingId(String workingId) {
		this.workingId = workingId;
	}
	
}

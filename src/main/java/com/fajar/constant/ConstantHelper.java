package com.fajar.constant;

public class ConstantHelper {
	
	//PROFILE NAME
	public static final String CONST_NAME_FAJAR				= "FAJAR";
	
	//GENERAL
	public static final String CONST_TEXT_TIMESTAMP			= "Timestamp";
	public static final String CONST_TEXT_CLIENT_ID			= "Client_Id";
	public static final String CONST_TEXT_SIGNATURE			= "Signature";
	public static final String CONST_TEXT_NULL_UPPERCASE	= "NULL";
	
	//NEED
	public static final String CONST_NEED_CONNECTOR			= "CONNECTOR";

	//FAJAR PROFILE HELPER
	public static final String CONST_CONNECTOR_FAJAR_CLIENT_ID	= "CLIENT_ID";
	public static final String CONST_CONNECTOR_FAJAR_SECRET_KEY	= "SECRET_KEY";
}

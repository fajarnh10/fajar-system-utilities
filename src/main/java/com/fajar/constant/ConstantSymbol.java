package com.fajar.constant;

public class ConstantSymbol {
	
	public static final String CONST_SYMBOL_EMPTY						= "\"\"";
	public static final String CONST_SYMBOL_DOT							= ".";
	public static final String CONST_SYMBOL_ISO_8601_YYYYMM 			= "yyyy-MM";
	public static final String CONST_SYMBOL_ISO_8601_YYYYMMDD 			= "yyyy-MM-dd";
	public static final String CONST_SYMBOL_ISO_8601_YYYYMMDDHHMMSS 	= "yyyy-MM-dd hh:mm:ss";
	
}

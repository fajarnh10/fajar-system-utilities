package com.fajar.constant;

public class ConstantCodes {

	//Integer
	public static final int CONST_INT_CODE_FALSE                    			= 1;
	public static final int CONST_INT_CODE_TRUE         						= 0;
	
	public static final String CONST_STRING_CODE_FALSE                    		= "false";
	public static final String CONST_STRING_CODE_TRUE                    		= "true";
	
	//FAJAR PROFILE API
	public static final String CONST_STRING_CODE_SUCCESS		 	    = "000";
	public static final String CONST_STRING_CODE_REQUEST_BODY_NULL      = "101";
	public static final String CONST_STRING_CODE_TIMESTAMP_NULL			= "111";
	public static final String CONST_STRING_CODE_CLIENT_ID_NULL			= "112";
	public static final String CONST_STRING_CODE_SIGNATURE_NULL			= "113";
	public static final String CONST_STRING_CODE_WORKING_ID_NULL		= "121";
	public static final String CONST_STRING_CODE_DATA_NOT_FOUND			= "211";
	public static final String CONST_STRING_CODE_GENERAL_ERROR          = "999";
	public static final String CONST_STRING_CODE_SIGNATURE_NOT_MATCH    = "913";
	
}
